package az.bdc.management.service;

import az.bdc.management.dao.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    boolean update(User user);

    boolean delete(int id);

    boolean insert(User u);

    User getById(int id);


    User getByEmail(String email);

    User getByPassword(String password);

    User getByPhoneNumber(String phoneNumber);


}
