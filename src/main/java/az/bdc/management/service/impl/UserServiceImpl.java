package az.bdc.management.service.impl;

import az.bdc.management.dao.config.DatabaseConnection;
import az.bdc.management.dao.User;
import az.bdc.management.service.UserService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {


    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        try (Connection connection = DatabaseConnection.connect();
             Statement statement = connection.createStatement()) {
            statement.execute("select * from user");
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String username = resultSet.getString(2);
                String password = resultSet.getString(3);
                String email = resultSet.getString(4);
                String phoneNumber = resultSet.getString(5);

                User user = new User(id, username, password, email, phoneNumber);
                users.add(user);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public boolean update(User user) {
        try (Connection connection = DatabaseConnection.connect();
             PreparedStatement statement = connection.prepareStatement("update user set(username, password, email, phone_number) values (?, ?, ?, ?) where id = ?")) {
            statement.setInt(5, user.getId());
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPhoneNumber());
            statement.executeUpdate();
            return true;
        } catch (SQLException  | ClassNotFoundException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(int id) {
        try (Connection connection = DatabaseConnection.connect();
             PreparedStatement statement = connection.prepareStatement("delete from user where id = ?")) {
            statement.setInt(1, id);
            statement.executeUpdate();
            return true;
        } catch (SQLException  | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean insert(User u) {
        try (Connection connection = DatabaseConnection.connect();
             PreparedStatement statement = connection.prepareStatement("insert into user(username, password, email, phone_number) values(?,?,?,?)")) {
            statement.setString(1, u.getUsername());
            statement.setString(2, u.getPassword());
            statement.setString(3, u.getEmail());
            statement.setString(4, u.getPhoneNumber());
            statement.executeUpdate();
            return true;
        } catch (SQLException  | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public User getById(int id) {
        try (Connection connection = DatabaseConnection.connect();
             PreparedStatement statement = connection.prepareStatement("select * from user where id = ?")) {
            statement.setInt(1, id);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            User user = null;
            while (resultSet.next()) {
                user = new User(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5));
            }
            return user;
        } catch (SQLException  | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }



    public User getByPassword(String password) {
        try (Connection connection = DatabaseConnection.connect();
             PreparedStatement statement = connection.prepareStatement("select * from user where password = ?")) {
            statement.setString(1, password);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            User user = null;
            while (resultSet.next()) {
                user = new User(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5));
            }
            return user;
        } catch (SQLException  | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Connection connection = DatabaseConnection.connect();
             PreparedStatement statement = connection.prepareStatement("select * from user where email = ?")) {
            statement.setString(1, email);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            User user = null;
            while (resultSet.next()) {
                user = new User(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5));
            }
            return user;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public User getByPhoneNumber(String phoneNumber) {
        try (Connection connection = DatabaseConnection.connect();
             PreparedStatement statement = connection.prepareStatement("select * from user where phone_number = ?")) {
            statement.setString(1, phoneNumber);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            User user = null;
            while (resultSet.next()) {
                user = new User(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5));
            }
            return user;
        } catch (SQLException  | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}


