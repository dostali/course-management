package az.bdc.management.dao.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    public static Connection connect() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:5340/course_management";
        String username = "root";
        String password = "123456";
        return DriverManager.getConnection(url, username, password);
    }
}
